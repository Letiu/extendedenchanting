package letiu.extended_enchanting.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import letiu.extended_enchanting.core.ExtendedEnchanting;
import letiu.extended_enchanting.render.IconInfo;
import letiu.extended_enchanting.tiles.TileEnchantingTable;
import net.minecraft.block.BlockEnchantmentTable;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BEnchantingTable extends BlockEnchantmentTable {

    private IIcon top, bottom;

    public BEnchantingTable() {
        super();
        this.setBlockName("Enchanting Table");
        this.setCreativeTab(ExtendedEnchanting.modTab);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int p2) {
        return new TileEnchantingTable();
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
        player.openGui(ExtendedEnchanting.instance, 0, world, x, y, z);
        return true;
    }

    @Override
    public void onBlockPlacedBy(World p_149689_1_, int p_149689_2_, int p_149689_3_, int p_149689_4_, EntityLivingBase p_149689_5_, ItemStack p_149689_6_) {

    }

    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister)
    {
        this.blockIcon = iconRegister.registerIcon(IconInfo.TEXTURE_LOCATION + IconInfo.ENCHANTING_TABLE_SIDE);
        this.top = iconRegister.registerIcon(IconInfo.TEXTURE_LOCATION + IconInfo.ENCHANTING_TABLE_TOP);
        this.bottom = iconRegister.registerIcon(IconInfo.TEXTURE_LOCATION + IconInfo.ENCHANTING_TABLE_BOTTOM);
    }

    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta)
    {
        return side == 0 ? this.bottom : (side == 1 ? this.top : this.blockIcon);
    }
}
