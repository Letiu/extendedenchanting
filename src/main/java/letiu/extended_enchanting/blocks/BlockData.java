package letiu.extended_enchanting.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import letiu.extended_enchanting.core.ModInformation;
import net.minecraft.block.Block;

public class BlockData {

    public static Block enchantingTable;

    public static void init() {
        GameRegistry.registerBlock(enchantingTable = new BEnchantingTable(), ModInformation.ID + "_" + enchantingTable.getUnlocalizedName());
    }

    public static void addNames() {
        LanguageRegistry.addName(enchantingTable, enchantingTable.getUnlocalizedName());
    }

}
