package letiu.extended_enchanting.config;

import net.minecraftforge.common.config.Configuration;

import java.io.File;

public class ConfigHandler {

	private static final String CAREGORY_TWEAKS = "Tweaks";
	
	public static void init(File file) {
		Configuration config = new Configuration(file);
		
		try {
			config.load();
			
			ConfigData.disableVanillaEnchantmentTable = config.get(CAREGORY_TWEAKS, "disableVanillaEnchantmentTable", ConfigData.disableVanillaEnchantmentTable, "disables recipe for vanilla enchantment table").getBoolean(ConfigData.disableVanillaEnchantmentTable);

		}
		catch(Exception e) {
			System.out.println("Extended Enchanting: Exception trying to load the config file!");
		}
		finally {
			config.save();
		}
	}
}
