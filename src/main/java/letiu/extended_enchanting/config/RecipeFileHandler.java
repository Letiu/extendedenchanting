package letiu.extended_enchanting.config;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBuf;
import letiu.extended_enchanting.recipes.EnchantRecipe;
import letiu.extended_enchanting.recipes.EnchantRecipeCollector;
import net.minecraft.entity.DataWatcher;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

import java.io.*;

public class RecipeFileHandler {

    private static File file;

    public static void init(File _file) {
        file = _file;
    }

    public static void tryLoad() {
       if (file != null) {
           if (file.exists()) {
               try {
                   FileInputStream in = new FileInputStream(file);
                   NBTTagCompound nbt = CompressedStreamTools.readCompressed(in);

//                   NBTTagCompound nbt = CompressedStreamTools.read(file);

                   NBTTagList nbt_recipe_list = nbt.getTagList("recipe_list", 10);

                   for (int i = 0; i < nbt_recipe_list.tagCount(); i++) {
                       EnchantRecipe recipe = EnchantRecipe.getEmptyRecipe();
                       recipe.readFromNBT(nbt_recipe_list.getCompoundTagAt(i));
                       EnchantRecipeCollector.addRecipe(recipe);
                   }
                   in.close();
               }
               catch (FileNotFoundException e) {
                   e.printStackTrace();
               }
               catch (IOException e) {
                   e.printStackTrace();
               }
           }
           else {
               System.out.println("FAILED LOADING RECIPES: File doesn't exist");
           }
        }
        else {
           System.out.println("FAILED LOADING RECIPES: File was NULL");
       }
    }

    public static void save() {
        if (file != null) {
            NBTTagCompound nbt = new NBTTagCompound();

            NBTTagList nbt_recipe_list = new NBTTagList();

            for (EnchantRecipe recipe : EnchantRecipeCollector.getRecipes()) {
                nbt_recipe_list.appendTag(recipe.writeToNBT());
            }

            nbt.setTag("recipe_list", nbt_recipe_list);

            try {
                if (!file.exists()) file.createNewFile();
                FileOutputStream out = new FileOutputStream(file);

                CompressedStreamTools.writeCompressed(nbt, out);
                //CompressedStreamTools.write(nbt, file);
                out.close();
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
