package letiu.extended_enchanting.core;

import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import letiu.extended_enchanting.blocks.BlockData;
import letiu.extended_enchanting.config.ConfigHandler;
import letiu.extended_enchanting.config.RecipeFileHandler;
import letiu.extended_enchanting.enchantments.PotionEnchantment;
import letiu.extended_enchanting.gui.GuiHandler;
import letiu.extended_enchanting.integration.nei.RecipeHandlers;
import letiu.extended_enchanting.items.ItemData;
import letiu.extended_enchanting.network.PacketData;
import letiu.extended_enchanting.proxies.CommonProxy;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.EnchantmentData;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraftforge.common.ForgeHooks;

import java.io.File;

@Mod(modid = ModInformation.ID, name = ModInformation.NAME, version = ModInformation.VERSION)
public class ExtendedEnchanting {

    @Instance(ModInformation.ID)
    public static ExtendedEnchanting instance;

    @SidedProxy(clientSide = "letiu.extended_enchanting.proxies.ClientProxy", serverSide = "letiu.extended_enchanting.proxies.CommonProxy")
    public static CommonProxy proxy;

    public static CreativeTabs modTab;
    public static final PotionEnchantment POTION_ENCHANTMENT = new PotionEnchantment(124, 100, EnumEnchantmentType.armor);


    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        ConfigHandler.init(event.getSuggestedConfigurationFile());

        String configDirPath = event.getModConfigurationDirectory().getPath();

        RecipeFileHandler.init(new File(configDirPath + "\\extended_enchanting_recipes.dat"));

        modTab = new ModTab(ModInformation.NAME);

        BlockData.init();
        ItemData.init();

        proxy.initSounds();
        proxy.initRenderes();
        proxy.addRecipes();
    }

    @EventHandler
    public void load(FMLInitializationEvent event) {

        BlockData.addNames();
        ItemData.addNames();

        Recipes.registerNormalRecipes();

        proxy.registerEvents();
        proxy.registerTileEntities();

        NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());

        PacketData.init();


    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        RecipeFileHandler.tryLoad();

        if (event.getSide() == Side.CLIENT && Loader.isModLoaded("NotEnoughItems"))
            RecipeHandlers.registerHandlers();
    }
}