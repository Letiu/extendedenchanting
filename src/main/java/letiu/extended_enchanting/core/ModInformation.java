package letiu.extended_enchanting.core;

public class ModInformation {

	public static final String ID = "extended_enchanting";
	public static final String NAME = "Extended Enchanting";
	public static final String VERSION = "0.0.7";
	public static final String CHANNEL = "extended_enchanting";
}
