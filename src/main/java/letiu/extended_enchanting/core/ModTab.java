package letiu.extended_enchanting.core;

import letiu.extended_enchanting.blocks.BlockData;
import letiu.extended_enchanting.items.ItemData;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.EnchantmentData;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class ModTab extends CreativeTabs {

	private ItemStack potionEnchantBook = Items.enchanted_book.getEnchantedItemStack(
			new EnchantmentData(ExtendedEnchanting.POTION_ENCHANTMENT, 0));

	public ModTab(String label) {
		super(label);
	}

	@Override
	public Item getTabIconItem() {
		return Item.getItemFromBlock(BlockData.enchantingTable);
	}
	
	@Override
	public ItemStack getIconItemStack() {
		return new ItemStack(BlockData.enchantingTable);
	}

	@Override
	public void displayAllReleventItems(List list) {
		super.displayAllReleventItems(list);
		list.add(potionEnchantBook);
	}
}
