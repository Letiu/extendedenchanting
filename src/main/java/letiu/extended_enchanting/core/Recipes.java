package letiu.extended_enchanting.core;

import cpw.mods.fml.common.registry.GameRegistry;
import letiu.extended_enchanting.blocks.BlockData;
import letiu.extended_enchanting.config.ConfigData;
import letiu.extended_enchanting.items.ItemData;
import letiu.extended_enchanting.util.CompareUtil;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

import java.util.Iterator;
import java.util.List;

public class Recipes {

    public static void registerNormalRecipes() {

        if (ConfigData.disableVanillaEnchantmentTable) removeVanillaRecipes();

        Block tableBase = ConfigData.disableVanillaEnchantmentTable ? Blocks.obsidian : Blocks.nether_brick;
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(BlockData.enchantingTable),
                " A ", "BCB", "CCC",
                'A', Items.book,
                'B', Items.diamond,
                'C', tableBase));

    }

    public static void removeVanillaRecipes() {
        List<IRecipe> recipes = CraftingManager.getInstance().getRecipeList();

        Iterator<IRecipe> it = recipes.iterator();

        while (it.hasNext()) {
            ItemStack stack = it.next().getRecipeOutput();
            if (stack != null && CompareUtil.compareIDs(stack, Blocks.enchanting_table))
                it.remove();
        };
    }

}
