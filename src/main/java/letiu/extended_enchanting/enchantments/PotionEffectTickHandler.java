package letiu.extended_enchanting.enchantments;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import letiu.extended_enchanting.core.ExtendedEnchanting;
import letiu.extended_enchanting.util.NumberMagic;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

public class PotionEffectTickHandler {

    @SubscribeEvent
    public void handleEvent(TickEvent.PlayerTickEvent event) {

        for (int i = 0; i < 4; i++) {
            ItemStack armor = event.player.getCurrentArmor(i);
            if (armor != null && armor.hasTagCompound()) {
                NBTTagList enchantTagList = armor.getEnchantmentTagList();
                if (enchantTagList != null) {
                    for (int j = 0; j < enchantTagList.tagCount(); j++) {

                        short id = enchantTagList.getCompoundTagAt(j).getShort("id");
                        if (id == ExtendedEnchanting.POTION_ENCHANTMENT.effectId) {

                            short level = enchantTagList.getCompoundTagAt(j).getShort("lvl");
                            int effectID = NumberMagic.getPotionID(level);
                            int potionStrength = NumberMagic.getPotionStrength(level);
                            if (level != 0) event.player.addPotionEffect(new PotionEffect(effectID, 30, potionStrength));
                        }
                    }
                }
            }
        }
    }

}
