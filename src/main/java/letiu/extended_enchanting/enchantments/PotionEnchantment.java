package letiu.extended_enchanting.enchantments;

import letiu.extended_enchanting.core.ExtendedEnchanting;
import letiu.extended_enchanting.util.NumberMagic;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.potion.Potion;
import net.minecraft.util.StatCollector;

public class PotionEnchantment extends Enchantment {

    public static void addEnchantment(ItemStack stack, int enchantStrength) {

        if (stack == null) return;

        if (stack.stackTagCompound == null)
        {
            stack.setTagCompound(new NBTTagCompound());
        }

        if (!stack.stackTagCompound.hasKey("ench", 9))
        {
            stack.stackTagCompound.setTag("ench", new NBTTagList());
        }

        NBTTagList nbttaglist = stack.stackTagCompound.getTagList("ench", 10);
        NBTTagCompound nbttagcompound = new NBTTagCompound();
        nbttagcompound.setShort("id", (short) ExtendedEnchanting.POTION_ENCHANTMENT.effectId);
        // no longer stupidly cast to byte
        nbttagcompound.setShort("lvl", (short) enchantStrength);
        nbttaglist.appendTag(nbttagcompound);
    }

    public PotionEnchantment(int id, int rarity, EnumEnchantmentType type) {
        super(id, rarity, type);
        this.setName("Potion Enchantment");
        //addToBookList(this);
    }

    @Override
    public String getTranslatedName(int enchantLevel) {
        int potionID = NumberMagic.getPotionID(enchantLevel);
        int potionStrength = NumberMagic.getPotionStrength(enchantLevel);

        if (potionID >= 0 && potionID < Potion.potionTypes.length) {
            Potion potion = Potion.potionTypes[potionID];
            if (potion != null) {
                return StatCollector.translateToLocal(potion.getName()) + " "
                        + StatCollector.translateToLocal("enchantment.level." + (potionStrength + 1));
            }
        }

        return "Potion Effect";
    }

    @Override
    public boolean canApplyAtEnchantingTable(ItemStack stack) {
        return false;
    }

    @Override
    public int getMinEnchantability(int par1) {
        return 5 + (par1 - 1) * 10;
    }

    @Override
    public int getMaxEnchantability(int par1) {
        return this.getMinEnchantability(par1) + 20;
    }

    @Override
    public int getMaxLevel() {
        return Integer.MAX_VALUE;
    }
}
