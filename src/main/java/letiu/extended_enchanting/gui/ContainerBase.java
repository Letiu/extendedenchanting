package letiu.extended_enchanting.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public abstract class ContainerBase extends Container {

    protected void bindPlayerInventory(InventoryPlayer inventoryPlayer, int xOff, int yOff) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 9; j++) {
                addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9, 8 + j * 18 + xOff, 84 + i * 18 + yOff));
            }
        }

        for (int i = 0; i < 9; i++) {
            addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18 + xOff, 142 + yOff));
        }
    }

    protected void bindPlayerInventory(InventoryPlayer inventoryPlayer) {
        this.bindPlayerInventory(inventoryPlayer, 0, 0);
    }

    /** called by ContainerBase */
    public void inventoryChanged() {

    }

    @Override
    public ItemStack slotClick(int p_75144_1_, int p_75144_2_, int p_75144_3_, EntityPlayer p_75144_4_) {
        ItemStack result = super.slotClick(p_75144_1_, p_75144_2_, p_75144_3_, p_75144_4_);

        inventoryChanged();

        return result;
    }

    public abstract int getInventorySize();

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot) {

        ItemStack stack = null;
        Slot slotObject = (Slot) inventorySlots.get(slot);
        int invSize = this.getInventorySize();

        // null checks and checks if the item can be stacked (maxStackSize > 1)
        if (slotObject != null && slotObject.getHasStack()) {
            ItemStack stackInSlot = slotObject.getStack();
            stack = stackInSlot.copy();

            // merges the item into player inventory since its in the tileEntity
            if (slot < invSize) {
                if (!this.mergeItemStack(stackInSlot, invSize, 35, true)) {
                    return null;
                }
            }
            // place the item into the tileEntity since its in the player inventory
            else {
                if (!this.mergeItemStack(stackInSlot, 0, invSize, false)) {
                    return null;
                }
            }

            if (stackInSlot.stackSize == 0) {
                slotObject.putStack(null);
            } else {
                slotObject.onSlotChanged();
            }

            if (stackInSlot.stackSize == stack.stackSize) {
                return null;
            }
            slotObject.onPickupFromSlot(player, stackInSlot);
        }
        return null;
    }

    private int getMaxStackSize(Slot slot, ItemStack stack) {
        int slotMaxSize = slot.getSlotStackLimit();
        int stackMaxSize = stack.getMaxStackSize();
        return slotMaxSize < stackMaxSize ? slotMaxSize : stackMaxSize;
    }

    /*
        Vanilla version doesn't take slot limit into account. °-°
     */
    @Override
    protected boolean mergeItemStack(ItemStack stack, int minSlot, int maxSlot, boolean countDown)
    {
        boolean flag1 = false;
        int k = minSlot;

        if (countDown)
        {
            k = maxSlot - 1;
        }

        Slot slot;
        ItemStack itemstack1;

        if (stack.isStackable())
        {
            while (stack.stackSize > 0 && (!countDown && k < maxSlot || countDown && k >= minSlot))
            {
                slot = (Slot)this.inventorySlots.get(k);
                itemstack1 = slot.getStack();

                if (itemstack1 != null && itemstack1.getItem() == stack.getItem() && (!stack.getHasSubtypes() || stack.getItemDamage() == itemstack1.getItemDamage()) && ItemStack.areItemStackTagsEqual(stack, itemstack1))
                {
                    int l = itemstack1.stackSize + stack.stackSize;

                    if (l <= getMaxStackSize(slot, stack))
                    {
                        stack.stackSize = 0;
                        itemstack1.stackSize = l;
                        slot.onSlotChanged();
                        flag1 = true;
                    }
                    else if (itemstack1.stackSize < getMaxStackSize(slot, stack))
                    {
                        stack.stackSize -= getMaxStackSize(slot, stack) - itemstack1.stackSize;
                        itemstack1.stackSize = getMaxStackSize(slot, stack);
                        slot.onSlotChanged();
                        flag1 = true;
                    }
                }

                if (countDown)
                {
                    --k;
                }
                else
                {
                    ++k;
                }
            }
        }

        if (stack.stackSize > 0)
        {
            if (countDown)
            {
                k = maxSlot - 1;
            }
            else
            {
                k = minSlot;
            }

            while (!countDown && k < maxSlot || countDown && k >= minSlot)
            {
                slot = (Slot)this.inventorySlots.get(k);
                itemstack1 = slot.getStack();

                if (itemstack1 == null)
                {
                    itemstack1 = stack.copy();
                    itemstack1.stackSize = 0;

                    int l1 = stack.stackSize;

                    if (l1 <= getMaxStackSize(slot, stack))
                    {
                        stack.stackSize = 0;
                        itemstack1.stackSize = l1;
                        slot.putStack(itemstack1);
                        slot.onSlotChanged();
                        flag1 = true;
                    }
                    else if (itemstack1.stackSize < getMaxStackSize(slot, stack))
                    {
                        stack.stackSize -= getMaxStackSize(slot, stack) - itemstack1.stackSize;
                        itemstack1.stackSize = getMaxStackSize(slot, stack);
                        slot.putStack(itemstack1);
                        slot.onSlotChanged();
                        flag1 = true;
                    }

                    break;
                }

                if (countDown)
                {
                    --k;
                }
                else
                {
                    ++k;
                }
            }
        }

        return flag1;
    }
}
