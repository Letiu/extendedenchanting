package letiu.extended_enchanting.gui;

import cpw.mods.fml.common.network.IGuiHandler;
import letiu.extended_enchanting.gui.container.*;
import letiu.extended_enchanting.gui.gui.*;
import letiu.extended_enchanting.tiles.TileEnchantingTable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class GuiHandler implements IGuiHandler {

    public static final int ENCHANTING_CODEX_ID = 2;
    public static final int CODEX_CREATE_ID = 3;
    public static final int CODEX_CREATE_ENCHANT_ID = 4;
    public static final int CODEX_CREATE_POTION_ID = 5;
    public static final int BLACK_WHITE_LIST_ID = 6;
    public static final int RESTORE_PREVIOUS_ID = 7;

    // Used for restoring previous gui upon return from sub guis
    public static Object tempGui = null;
    public static Object tempCon = null;

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {

        if (ID == ENCHANTING_CODEX_ID) return new ConEnchantingCodex(player.inventory);
        else if (ID == CODEX_CREATE_ID) {
            ConCodexCreate con = new ConCodexCreate(player.inventory);
            if (x != -1) con.loadRecipe(x);
            return con;
        }
        else if (ID == CODEX_CREATE_ENCHANT_ID || ID == CODEX_CREATE_POTION_ID) {
            ConCodexCreateEnchant con = new ConCodexCreateEnchant(player.inventory);
            if (x != -1) con.loadRecipe(x);
            tempCon = con;
            return con;
        }
        else if (ID == BLACK_WHITE_LIST_ID) {
            ConBlackWhiteList con = new ConBlackWhiteList(player.inventory);
            if (x != -1) con.loadRecipe(x);
            return con;
        }
        else if (ID == RESTORE_PREVIOUS_ID) {
            return tempCon;
        }
        else {
            TileEntity tile = world.getTileEntity(x, y, z);
            if (tile instanceof TileEnchantingTable) {
                return new ConEnchantingTable(player.inventory, ((TileEnchantingTable) tile));
            }
        }

        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {

        if (ID == ENCHANTING_CODEX_ID) return new GuiEnchantingCodex(new ConEnchantingCodex(player.inventory));
        else if (ID == CODEX_CREATE_ID) {
            ConCodexCreate con = new ConCodexCreate(player.inventory);
            if (x != -1) con.loadRecipe(x);
            GuiCodexCreate gui = new GuiCodexCreate(con);
            if (x != -1) gui.loadRecpie(x);
            return gui;
        }
        else if (ID == CODEX_CREATE_ENCHANT_ID) {
            ConCodexCreateEnchant con = new ConCodexCreateEnchant(player.inventory);
            if (x != -1) con.loadRecipe(x);
            GuiCodexCreateEnchant gui = new GuiCodexCreateEnchant(con);
            if (x != -1) gui.loadRecpie(x);
            tempCon = con;
            tempGui = gui;
            return gui;
        }
        else if (ID == CODEX_CREATE_POTION_ID) {
            ConCodexCreateEnchant con = new ConCodexCreateEnchant(player.inventory);
            if (x != -1) con.loadRecipe(x);
            GuiCodexCreatePotion gui = new GuiCodexCreatePotion(con);
            if (x != -1) gui.loadRecpie(x);
            tempCon = con;
            tempGui = gui;
            return gui;
        }
        else if (ID == BLACK_WHITE_LIST_ID) {
            ConBlackWhiteList con = new ConBlackWhiteList(player.inventory);
            if (x != -1) con.loadRecipe(x);
            GuiBlackWhiteList gui = new GuiBlackWhiteList(con);
            if (x != -1) gui.loadRecpie(x);
            return gui;

        }
        else if (ID == RESTORE_PREVIOUS_ID) {
            return tempGui;
        }
        else {
            TileEntity tile = world.getTileEntity(x, y, z);
            if (tile instanceof TileEnchantingTable) {
                return new GuiEnchantingTable(new ConEnchantingTable(player.inventory, (TileEnchantingTable) tile), (TileEnchantingTable) tile);
            }
        }

        return null;
    }
}
