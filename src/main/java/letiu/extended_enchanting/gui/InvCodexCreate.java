package letiu.extended_enchanting.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class InvCodexCreate implements IInventory {

    private ItemStack[] inventory;

    public InvCodexCreate(int size) {
        this.inventory = new ItemStack[size];
    }

    @Override
    public int getSizeInventory() {
        return inventory.length;
    }

    @Override
    public ItemStack getStackInSlot(int i) {
        if (i < 0 || i >= getSizeInventory()) return null;
        return inventory[i];
    }

    @Override
    public ItemStack decrStackSize(int i, int amt) {
        if (i < 0 || i >= inventory.length) return null;

        ItemStack stack = getStackInSlot(i);

        if (stack != null) {
            if (stack.stackSize <= amt) {
                setInventorySlotContents(i, null);
            }
            else {
                stack = stack.splitStack(amt);
                if (stack.stackSize == 0) {
                    setInventorySlotContents(i, null);
                }
            }
        }

        return stack;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int i) {
        return getStackInSlot(i);
    }

    @Override
    public void setInventorySlotContents(int i, ItemStack stack) {
        if (i < 0 || i >= inventory.length) return;
        inventory[i] = stack;
        if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
            stack.stackSize = this.getInventoryStackLimit();
        }
    }

    @Override
    public String getInventoryName() {
        return null;
    }

    @Override
    public boolean hasCustomInventoryName() {
        return false;
    }

    @Override
    public int getInventoryStackLimit() {
        return 1;
    }

    @Override
    public void markDirty() {

    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer player) {
        return true;
    }

    @Override
    public void openInventory() {

    }

    @Override
    public void closeInventory() {

    }

    @Override
    public boolean isItemValidForSlot(int i, ItemStack stack) {
        return true;
    }
}
