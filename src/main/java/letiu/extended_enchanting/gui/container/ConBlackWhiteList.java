package letiu.extended_enchanting.gui.container;

import letiu.extended_enchanting.gui.ContainerBase;
import letiu.extended_enchanting.gui.GuiHandler;
import letiu.extended_enchanting.gui.InvCodexCreate;
import letiu.extended_enchanting.gui.gui.GuiCodexCreateEnchant;
import letiu.extended_enchanting.gui.gui.GuiCodexCreatePotion;
import letiu.extended_enchanting.recipes.EnchantRecipe;
import letiu.extended_enchanting.recipes.EnchantRecipeCollector;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ConBlackWhiteList extends ContainerBase {

    private InvCodexCreate inventory = new InvCodexCreate(18);

    public ConBlackWhiteList(InventoryPlayer inventoryPlayer) {

        int slot = 0;

        int x = 19 + 16;
        int y = 18;

        for (int a = 0; a < 3; a++) {
            for (int b = 0; b < 6; b++) {
                addSlotToContainer(new Slot(inventory, slot++, x + b * 18, y + a * 18));
            }
        }

        bindPlayerInventory(inventoryPlayer);
    }

    public InvCodexCreate getRecipeInventory() {
        return inventory;
    }

    @Override
    public int getInventorySize() {
        return inventory.getSizeInventory();
    }

    @Override
    public boolean canInteractWith(EntityPlayer player) {
        return true;
    }

    public void loadRecipe(int recipeID) {

        Object parentGui = GuiHandler.tempGui;

        if (parentGui == null) return;

        ItemStack[] list = null;
        if (parentGui instanceof GuiCodexCreateEnchant) {
            list = ((GuiCodexCreateEnchant) parentGui).getBlackWhiteList();
        }
        else if (parentGui instanceof GuiCodexCreatePotion) {
            list = ((GuiCodexCreatePotion) parentGui).getBlackWhiteList();
        }

        if (list == null) return;

        int i = 0;
        for (ItemStack stack : list) {
            if (stack != null) {
                inventory.setInventorySlotContents(i++, stack);
            }
        }
    }
}
