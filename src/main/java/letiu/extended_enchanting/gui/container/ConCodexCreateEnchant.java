package letiu.extended_enchanting.gui.container;

import letiu.extended_enchanting.gui.ContainerBase;
import letiu.extended_enchanting.gui.InvCodexCreate;
import letiu.extended_enchanting.recipes.EnchantRecipe;
import letiu.extended_enchanting.recipes.EnchantRecipeCollector;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ConCodexCreateEnchant extends ContainerBase {

    private InvCodexCreate inventory = new InvCodexCreate(9);

    public ConCodexCreateEnchant(InventoryPlayer inventoryPlayer) {

        int slot = 0;

        int x = 80;
        int y = 34;
        int spacingA = 28;
        int spacingB = 22;

        addSlotToContainer(new Slot(inventory, slot++, x, y));
        addSlotToContainer(new Slot(inventory, slot++, x - spacingA, y));
        addSlotToContainer(new Slot(inventory, slot++, x - spacingB, y - spacingB));
        addSlotToContainer(new Slot(inventory, slot++, x, y - spacingA));
        addSlotToContainer(new Slot(inventory, slot++, x + spacingB, y - spacingB));
        addSlotToContainer(new Slot(inventory, slot++, x + spacingA, y));
        addSlotToContainer(new Slot(inventory, slot++, x + spacingB, y + spacingB));
        addSlotToContainer(new Slot(inventory, slot++, x, y + spacingA));
        addSlotToContainer(new Slot(inventory, slot++, x - spacingB, y + spacingB));

        //addSlotToContainer(new Slot(inventory, slot++, x + spacingB + 40, y + spacingB));

        bindPlayerInventory(inventoryPlayer);
    }

    public InvCodexCreate getRecipeInventory() {
        return inventory;
    }

    @Override
    public int getInventorySize() {
        return inventory.getSizeInventory();
    }

    @Override
    public boolean canInteractWith(EntityPlayer player) {
        return true;
    }

    public void loadRecipe(int recipeID) {
        if (EnchantRecipeCollector.getRecipes().size() > recipeID && recipeID >= 0) {
            EnchantRecipe recipe = EnchantRecipeCollector.getRecipes().get(recipeID);
            //inventory.setInventorySlotContents(0, recipe.getBase());
            inventory.setInventorySlotContents(0, recipe.getResult());
            int i = 1;
            for (ItemStack catalyst : recipe.getCatalysts()) {
                inventory.setInventorySlotContents(i++, catalyst);
            }
        }
    }
}
