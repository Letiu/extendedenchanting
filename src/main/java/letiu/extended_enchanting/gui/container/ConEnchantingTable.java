package letiu.extended_enchanting.gui.container;

import letiu.extended_enchanting.core.ExtendedEnchanting;
import letiu.extended_enchanting.gui.ContainerBase;
import letiu.extended_enchanting.gui.gui.GuiEnchantingTable;
import letiu.extended_enchanting.recipes.EnchantRecipe;
import letiu.extended_enchanting.recipes.EnchantRecipeCollector;
import letiu.extended_enchanting.tiles.TileEnchantingTable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;

public class ConEnchantingTable extends ContainerBase {

    private TileEnchantingTable tile;
    private GuiEnchantingTable gui;

    public ConEnchantingTable(InventoryPlayer inventoryPlayer, TileEnchantingTable tile) {

        this.tile = tile;

        int slot = 0;

        int x = 80;
        int y = 34 + 38 / 2 - 4;
        int spacingA = 26;
        int spacingB = 19;

        addSlotToContainer(new Slot(tile, slot++, x, y));
        addSlotToContainer(new Slot(tile, slot++, x - spacingA + 1, y));
        addSlotToContainer(new Slot(tile, slot++, x - spacingB, y - spacingB));
        addSlotToContainer(new Slot(tile, slot++, x, y - spacingA));
        addSlotToContainer(new Slot(tile, slot++, x + spacingB, y - spacingB));
        addSlotToContainer(new Slot(tile, slot++, x + spacingA, y));
        addSlotToContainer(new Slot(tile, slot++, x + spacingB, y + spacingB));
        addSlotToContainer(new Slot(tile, slot++, x, y + spacingA));
        addSlotToContainer(new Slot(tile, slot++, x - spacingB, y + spacingB));

        bindPlayerInventory(inventoryPlayer, 0, 38);
    }

    @Override
    public boolean canInteractWith(EntityPlayer player) {
        return tile.isUseableByPlayer(player);
    }

    @Override
    public int getInventorySize() {
        return tile.getSizeInventory();
    }

    public void setGui(GuiEnchantingTable gui) {
        this.gui = gui;
    }

    @Override
    public void inventoryChanged() {
        if (gui != null) {
            EnchantRecipe recipe = EnchantRecipeCollector.findMatch(tile.getInventory(), ExtendedEnchanting.proxy.getPlayer(), false);
            if (recipe != null) {
                gui.previewRecipe(recipe);
            }
            else {
                gui.clearRecipePreview();
            }
        }
    }
}
