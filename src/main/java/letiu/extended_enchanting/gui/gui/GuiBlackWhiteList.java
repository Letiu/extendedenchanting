package letiu.extended_enchanting.gui.gui;

import letiu.extended_enchanting.core.ExtendedEnchanting;
import letiu.extended_enchanting.gui.GuiCustomButton;
import letiu.extended_enchanting.gui.GuiHandler;
import letiu.extended_enchanting.gui.GuiTransparentButton;
import letiu.extended_enchanting.gui.container.ConBlackWhiteList;
import letiu.extended_enchanting.render.IconInfo;
import letiu.extended_enchanting.util.Vector2;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemStack;

public class GuiBlackWhiteList extends GuiContainer {

    private GuiButton saveButton;
    private GuiCustomButton blackWhiteButton;
    private ConBlackWhiteList container;

    private GuiTransparentButton helmetButton;
    private GuiTransparentButton chestButton;
    private GuiTransparentButton legButton;
    private GuiTransparentButton bootButton;

    private boolean isEdit = false;
    private int editID = -1;
    private boolean isWhitelist = false;

    private boolean[] armorLocks = new boolean[4];

    public GuiBlackWhiteList(ConBlackWhiteList container) {
        super(container);
        this.container = container;
    }

    @Override
    public void initGui() {
        super.initGui();

        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;

        buttonList.add(saveButton = new GuiButton(0, x + 10, y + 10, 20, 20, "<"));
        buttonList.add(blackWhiteButton = new GuiCustomButton(1, x + 10, y + 32, 20, 20, "", new Vector2(40, 0), new Vector2(60, 0)));

        buttonList.add(helmetButton = new GuiTransparentButton(2, x + 148, y + 7, 20, 20, "", new Vector2(100, 0), new Vector2(80, 0)));
        buttonList.add(chestButton = new GuiTransparentButton(3, x + 148, y + 7 + 17, 20, 20, "", new Vector2(100, 0), new Vector2(80, 0)));
        buttonList.add(legButton = new GuiTransparentButton(4, x + 148, y + 7 + 17 * 2, 20, 20, "", new Vector2(100, 0), new Vector2(80, 0)));
        buttonList.add(bootButton = new GuiTransparentButton(5, x + 148, y + 7 + 17 * 3, 20, 20, "", new Vector2(100, 0), new Vector2(80, 0)));

        blackWhiteButton.setInverted(isWhitelist);

        helmetButton.setInverted(armorLocks[0]);
        chestButton.setInverted(armorLocks[1]);
        legButton.setInverted(armorLocks[2]);
        bootButton.setInverted(armorLocks[3]);
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == 0) {

            armorLocks[0] = helmetButton.isInverted();
            armorLocks[1] = chestButton.isInverted();
            armorLocks[2] = legButton.isInverted();
            armorLocks[3] = bootButton.isInverted();

            Object parentGui = GuiHandler.tempGui;

            ItemStack[] list = new ItemStack[18];
            for (int i = 0; i < 18; i++) {
                list[i] = container.getRecipeInventory().getStackInSlot(i);
            }

            if (parentGui instanceof GuiCodexCreateEnchant) {
                ((GuiCodexCreateEnchant) parentGui).setBlackWhiteListData(list, blackWhiteButton.isInverted());
            }
            else if (parentGui instanceof GuiCodexCreatePotion) {
                ((GuiCodexCreatePotion) parentGui).setBlackWhiteListData(list, blackWhiteButton.isInverted());
            }
            Minecraft.getMinecraft().thePlayer.openGui(ExtendedEnchanting.instance, GuiHandler.RESTORE_PREVIOUS_ID, null, 0, 0, 0);
        }
        else if (button.id == 1) {
            blackWhiteButton.invert();
            isWhitelist = blackWhiteButton.isInverted();
        }
        else if (button.id == 2) {
            helmetButton.invert();
        }
        else if (button.id == 3) {
            chestButton.invert();
        }
        else if (button.id == 4) {
            legButton.invert();
        }
        else if (button.id == 5) {
            bootButton.invert();
        }
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int param1, int param2) {
        //...
        /*
        this.mc.getTextureManager().bindTexture(IconInfo.BUTTON_ICONS_RL);
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        y -= 24;
        x += 24;
        this.drawTexturedModalRect(x, y, 80, 0, 20, 20);
        this.drawTexturedModalRect(x, y + 16, 80, 0, 20, 20);
        this.drawTexturedModalRect(x, y + 16 * 2, 80, 0, 20, 20);
        this.drawTexturedModalRect(x, y + 16 * 3, 80, 0, 20, 20);*/
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
        this.mc.getTextureManager().bindTexture(IconInfo.BLACK_WHITE_LIST_GUI_RL);
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
        this.drawTexturedModalRect(x, y - 15, 0, ySize, xSize, 15);
    }

    @Override
    public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_) {
        super.drawScreen(p_73863_1_, p_73863_2_, p_73863_3_);
        //...
    }

    public void loadRecpie(int recipeID) {

        Object parentGui = GuiHandler.tempGui;

        if (parentGui == null) return;

        if (parentGui instanceof GuiCodexCreateEnchant) {
            isWhitelist = ((GuiCodexCreateEnchant) parentGui).isWhitelist();
            armorLocks = ((GuiCodexCreateEnchant) parentGui).getArmorLocks();
        }
        else if (parentGui instanceof GuiCodexCreatePotion) {
            isWhitelist = ((GuiCodexCreatePotion) parentGui).isWhitelist();
            armorLocks = ((GuiCodexCreatePotion) parentGui).getArmorLocks();
        }
    }
}
