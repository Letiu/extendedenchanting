package letiu.extended_enchanting.gui.gui;

import letiu.extended_enchanting.config.RecipeFileHandler;
import letiu.extended_enchanting.core.ExtendedEnchanting;
import letiu.extended_enchanting.gui.GuiCustomButton;
import letiu.extended_enchanting.gui.GuiHandler;
import letiu.extended_enchanting.gui.container.ConCodexCreate;
import letiu.extended_enchanting.gui.container.ConCodexCreateEnchant;
import letiu.extended_enchanting.network.EditERecipePacket;
import letiu.extended_enchanting.network.PacketHandler;
import letiu.extended_enchanting.recipes.EnchantRecipe;
import letiu.extended_enchanting.recipes.EnchantRecipeCollector;
import letiu.extended_enchanting.render.IconInfo;
import letiu.extended_enchanting.util.Vector2;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemEnchantedBook;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import scala.Int;

import java.util.ArrayList;

public class GuiCodexCreateEnchant extends GuiContainer {

    private GuiButton saveButton;
    private GuiCustomButton neiButton;
    private GuiButton blackWhiteListButton;

    private ConCodexCreateEnchant container;
    private GuiTextField textFieldName;
    private GuiTextField textFieldLevel;
    private GuiTextField textFieldEnchStrength;

    private String recipeName = "";
    private int requiredLevels = 0;
    private boolean isEdit = false;
    private boolean isSet = false;
    private int editID = -1;
    private int enchStrength = 0;
    private boolean isHidden = false;
    private int recipeID = 0;

    private ItemStack[] blackWhiteList = null;
    private boolean isWhitelist = false;

    private boolean[] armorLocks = new boolean[4];

    public GuiCodexCreateEnchant(ConCodexCreateEnchant container) {
        super(container);
        this.container = container;
    }

    @Override
    public void initGui() {
        super.initGui();

        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;

        buttonList.add(saveButton = new GuiButton(0, x + 122, y + 10, 50, 20, "Save"));
        buttonList.add(blackWhiteListButton = new GuiButton(2, x + 147, y + 55, 20, 20, ">"));
        buttonList.add(neiButton = new GuiCustomButton(1, x + 147, y + 32, 20, 20, "", new Vector2(0, 0), new Vector2(20, 0)));

        textFieldName = new GuiTextField(this.fontRendererObj, x + 36, y - 11, 103, 12);
        textFieldName.setDisabledTextColour(-1);
        textFieldName.setEnableBackgroundDrawing(false);
        textFieldName.setMaxStringLength(40);
        textFieldName.setTextColor(0x4444EE);

        textFieldLevel = new GuiTextField(this.fontRendererObj, x + 5, y + 6 + 12, 40, 12);
        textFieldLevel.setTextColor(-1);
        textFieldLevel.setDisabledTextColour(-1);
        textFieldLevel.setEnableBackgroundDrawing(true);
        textFieldLevel.setMaxStringLength(40);

        textFieldEnchStrength = new GuiTextField(this.fontRendererObj, x + 5, y + 6 + 12 + 30, 40, 12);
        textFieldEnchStrength.setTextColor(-1);
        textFieldEnchStrength.setDisabledTextColour(-1);
        textFieldEnchStrength.setEnableBackgroundDrawing(true);
        textFieldEnchStrength.setMaxStringLength(40);

        if (isEdit || isSet) {
            textFieldLevel.setText("" + requiredLevels);
            textFieldEnchStrength.setText("" + enchStrength);
        }
        else {
            textFieldLevel.setText("" + 1);
            textFieldEnchStrength.setText("" + 1);
        }

        if (recipeName.equals("")) {
            textFieldName.setText("Recipe" + EnchantRecipeCollector.getRecipes().size());
        }
        else {
            textFieldName.setText(recipeName);
        }

        neiButton.setInverted(isHidden);
//        buttonList.add(expSlider = new GuiCustomSlider(1, x + 2, y + 4, 50, 20, 101, 1, "Lvls"));
//        if (isEdit) expSlider.setOption(requiredLevels);
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == 0) {

            int levels = 0;
            int enchStrength = 0;
            try {
                levels = Integer.valueOf(textFieldLevel.getText());
                enchStrength = Integer.valueOf(textFieldEnchStrength.getText());
            }
            catch (NumberFormatException e) {
                return;
            }

            if (textFieldName.getText().length() > 0) {

                //ItemStack base = container.getRecipeInventory().getStackInSlot(0);
                ItemStack result = container.getRecipeInventory().getStackInSlot(0);
                if (result != null && result.getItem() instanceof ItemEnchantedBook) {

                    ArrayList<ItemStack> catalysts = new ArrayList<ItemStack>();
                    for (int i = 0; i < 8; i++) {
                        ItemStack stack = container.getRecipeInventory().getStackInSlot(i + 1);
                        if (stack != null) catalysts.add(stack.copy());
                    }

                    ItemStack[] stacks = new ItemStack[catalysts.size()];
                    for (int i = 0; i < stacks.length; i++) {
                        stacks[i] = catalysts.get(i);
                    }

                    EnchantRecipe recipe = new EnchantRecipe(textFieldName.getText(), result.copy(), result.copy(),
                            levels, EnchantRecipe.RecipeType.ENCHANT, isHidden, enchStrength, isWhitelist, blackWhiteList, armorLocks, stacks);

                    if (!Minecraft.getMinecraft().isSingleplayer())
                        PacketHandler.sendToServer(new EditERecipePacket(recipe, editID));

                    if (isEdit) EnchantRecipeCollector.getRecipes().set(editID, recipe);
                    else EnchantRecipeCollector.addRecipe(recipe);

                    RecipeFileHandler.save();

                    Minecraft.getMinecraft().thePlayer.openGui(ExtendedEnchanting.instance, GuiHandler.ENCHANTING_CODEX_ID, null, 0, 0, 0);
                }
            }
        }
        else if (button.id == 1) {
            neiButton.invert();
            isHidden = neiButton.isInverted();
        }
        else if (button.id == 2) {
            recipeName = textFieldName.getText();
            requiredLevels = Integer.valueOf(textFieldLevel.getText());
            enchStrength = Integer.valueOf(textFieldEnchStrength.getText());
            isSet = true;
            Minecraft.getMinecraft().thePlayer.openGui(ExtendedEnchanting.instance, GuiHandler.BLACK_WHITE_LIST_ID, null, 0, 0, 0);
        }
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int param1, int param2) {
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        fontRendererObj.drawString("Levels:", 6, 6, 0x000000);
        fontRendererObj.drawString("EnchLvl:", 6, 6 + 30, 0x000000);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_) {
        this.mc.getTextureManager().bindTexture(IconInfo.CODEX_CREATE_ENCHANT_GUI_RL);
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
        this.drawTexturedModalRect(x, y - 15, 0, ySize, xSize, 15);
    }

    @Override
    public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_) {
        super.drawScreen(p_73863_1_, p_73863_2_, p_73863_3_);
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glDisable(GL11.GL_BLEND);
        textFieldName.drawTextBox();
        textFieldLevel.drawTextBox();
        textFieldEnchStrength.drawTextBox();
    }

    @Override
    protected void mouseClicked(int p_73864_1_, int p_73864_2_, int p_73864_3_) {
        super.mouseClicked(p_73864_1_, p_73864_2_, p_73864_3_);
        textFieldName.mouseClicked(p_73864_1_, p_73864_2_, p_73864_3_);
        textFieldLevel.mouseClicked(p_73864_1_, p_73864_2_, p_73864_3_);
        textFieldEnchStrength.mouseClicked(p_73864_1_, p_73864_2_, p_73864_3_);
    }

    protected void keyTyped(char p_73869_1_, int p_73869_2_) {
        if (!textFieldName.textboxKeyTyped(p_73869_1_, p_73869_2_)
                && !textFieldLevel.textboxKeyTyped(p_73869_1_, p_73869_2_)
                && !textFieldEnchStrength.textboxKeyTyped(p_73869_1_, p_73869_2_)) {
            super.keyTyped(p_73869_1_, p_73869_2_);
        }
    }

    public void setBlackWhiteListData(ItemStack[] list, boolean isWhitelsit) {
        this.blackWhiteList = list;
        this.isWhitelist = isWhitelsit;
    }

    public ItemStack[] getBlackWhiteList() {
        return blackWhiteList;
    }

    public boolean isWhitelist() {
        return isWhitelist;
    }

    public boolean[] getArmorLocks() {
        return armorLocks;
    }

    public void loadRecpie(int recipeID) {
        if (EnchantRecipeCollector.getRecipes().size() > recipeID && recipeID >= 0) {
            EnchantRecipe recipe = EnchantRecipeCollector.getRecipes().get(recipeID);
            this.recipeID = recipeID;
            recipeName = recipe.getName();
            requiredLevels = recipe.getRequiredLevels();
            isEdit = true;
            editID = recipeID;
            enchStrength = recipe.getEnchStrength();
            isHidden = recipe.isHidden();
            isWhitelist = recipe.isWhitelist();
            blackWhiteList = recipe.getBlackWhiteList();
            armorLocks = recipe.getArmorLocks();
        }
    }
}
