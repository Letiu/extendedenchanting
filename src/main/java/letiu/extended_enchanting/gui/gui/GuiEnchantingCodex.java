package letiu.extended_enchanting.gui.gui;

import cpw.mods.fml.client.GuiScrollingList;
import letiu.extended_enchanting.config.RecipeFileHandler;
import letiu.extended_enchanting.core.ExtendedEnchanting;
import letiu.extended_enchanting.network.PacketHandler;
import letiu.extended_enchanting.network.RemoveERecipePacket;
import letiu.extended_enchanting.network.RequestERecipeListPacket;
import letiu.extended_enchanting.recipes.EnchantRecipe;
import letiu.extended_enchanting.recipes.EnchantRecipeCollector;
import letiu.extended_enchanting.render.IconInfo;
import letiu.extended_enchanting.gui.GuiHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.inventory.Container;

import java.util.List;

public class GuiEnchantingCodex extends GuiContainer {

    private GuiScrollingList recipeList;
    private List<EnchantRecipe> recipes;
    private int selectedSlot = 0;

    private GuiButton createButton, editButton, removeButton, createEnchantButton;

    public GuiEnchantingCodex(Container container) {
        super(container);
        if (!Minecraft.getMinecraft().isSingleplayer())
            PacketHandler.sendToServer(new RequestERecipeListPacket());
    }

    @Override
    public void initGui() {
        super.initGui();

        recipes = EnchantRecipeCollector.getRecipes();

        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;

        buttonList.add(createButton = new GuiButton(0, x + 122, y + 10, 50, 20, "Recipe"));
        buttonList.add(createEnchantButton = new GuiButton(3, x + 122, y + 10 + 22, 50, 20, "Enchant"));
        buttonList.add(createEnchantButton = new GuiButton(4, x + 122, y + 10 + 22 * 2, 50, 20, "Potion"));
        buttonList.add(editButton = new GuiButton(1, x + 122, y + 10 + 22 * 3, 50, 20, "Edit"));
        buttonList.add(removeButton = new GuiButton(2, x + 122, y + 10 + 22 * 4, 50, 20, "Remove"));

        int listWidth = 108;
        final int spacingX = 10;

        recipeList = new GuiScrollingList(Minecraft.getMinecraft(), listWidth, 150, y + 10, y + 150, x + spacingX, 14) {

            @Override
            public void actionPerformed(GuiButton button) {
                super.actionPerformed(button);
            }

            @Override
            protected int getSize() {
                return recipes.size();
            }

            @Override
            protected void elementClicked(int index, boolean doubleClick) {
                selectedSlot = index;
            }

            @Override
            protected boolean isSelected(int index) {
                return selectedSlot == index;
            }

            @Override
            protected void drawBackground() {}

            @Override
            protected void drawSlot(int slot, int xPos, int yPos, int var4, Tessellator var5) {
                EnchantRecipe recipe = recipes.get(slot);
                boolean isEnchant = recipe.getRecipeType() == EnchantRecipe.RecipeType.ENCHANT;
                boolean isPotion = recipe.getRecipeType() == EnchantRecipe.RecipeType.POTION;
                int color = isPotion ? (slot == selectedSlot ? 0xFF44FF : 0xCC00CC) : (!isEnchant ? (slot == selectedSlot ? 0xFFFFFF : 0xAAAAAA) : (slot == selectedSlot ? 0x8888FF : 0x4444DD));
                mc.fontRenderer.drawString(recipe.getName(), xPos - listWidth + spacingX + 2, yPos + 1, color);
            }
        };

    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == 0) {
            Minecraft.getMinecraft().thePlayer.openGui(ExtendedEnchanting.instance, GuiHandler.CODEX_CREATE_ID, null, -1, -1, -1);
        }
        else if (button.id == 1) {
            EnchantRecipe recipe = recipes.get(selectedSlot);
            if (recipe.getRecipeType() == EnchantRecipe.RecipeType.GENERAL) {
                Minecraft.getMinecraft().thePlayer.openGui(ExtendedEnchanting.instance, GuiHandler.CODEX_CREATE_ID, null, selectedSlot, -1, -1);
            }
            else if (recipe.getRecipeType() == EnchantRecipe.RecipeType.ENCHANT) {
                Minecraft.getMinecraft().thePlayer.openGui(ExtendedEnchanting.instance, GuiHandler.CODEX_CREATE_ENCHANT_ID, null, selectedSlot, -1, -1);
            }
            else if (recipe.getRecipeType() == EnchantRecipe.RecipeType.POTION) {
                Minecraft.getMinecraft().thePlayer.openGui(ExtendedEnchanting.instance, GuiHandler.CODEX_CREATE_POTION_ID, null, selectedSlot, -1, -1);
            }
        }
        else if (button.id == 2) {
            recipes.remove(selectedSlot);
            if (!Minecraft.getMinecraft().isSingleplayer())
                PacketHandler.sendToServer(new RemoveERecipePacket(selectedSlot));
            RecipeFileHandler.save();
        }
        else if (button.id == 3) {
            Minecraft.getMinecraft().thePlayer.openGui(ExtendedEnchanting.instance, GuiHandler.CODEX_CREATE_ENCHANT_ID, null, -1, -1, -1);
        }
        else if (button.id == 4) {
            Minecraft.getMinecraft().thePlayer.openGui(ExtendedEnchanting.instance, GuiHandler.CODEX_CREATE_POTION_ID, null, -1, -1, -1);
        }
    }


    @Override
    protected void drawGuiContainerBackgroundLayer(float p1, int mouseX, int mouseY) {
        this.mc.getTextureManager().bindTexture(IconInfo.ENCHANTING_CODEX_GUI_RL);
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);

        recipeList.drawScreen(mouseX, mouseY, p1);
    }
}
