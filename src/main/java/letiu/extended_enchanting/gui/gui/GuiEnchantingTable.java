package letiu.extended_enchanting.gui.gui;

import letiu.extended_enchanting.core.ExtendedEnchanting;
import letiu.extended_enchanting.gui.container.ConEnchantingTable;
import letiu.extended_enchanting.network.PacketHandler;
import letiu.extended_enchanting.network.RequestERecipeListPacket;
import letiu.extended_enchanting.network.TryEnchantPacket;
import letiu.extended_enchanting.recipes.EnchantRecipe;
import letiu.extended_enchanting.render.IconInfo;
import letiu.extended_enchanting.tiles.TileEnchantingTable;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.ISound;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.audio.SoundManager;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiEnchantingTable extends GuiContainer {

    private TileEnchantingTable tile;
    private int requiredLevels = -1;
    boolean hasLevel = false;
    boolean checkedForRecipe = false;
    private ConEnchantingTable container;

    private GuiButton enchantButton;

    private static final int animationTicks = 100;
    private float animationTime = 0F;

    public GuiEnchantingTable(ConEnchantingTable container, TileEnchantingTable tile) {
        super(container);
        this.container = container;
        this.container.setGui(this);
        this.tile = tile;
        this.ySize = 204;
        if (!Minecraft.getMinecraft().isSingleplayer())
            PacketHandler.sendToServer(new RequestERecipeListPacket());
    }

    @Override
    public void initGui() {
        super.initGui();

        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;

        //buttonList.add(new GuiButton(0, x + 122, y + 10, 50, 20, "Enchant"));
        enchantButton = new GuiButton(0, x + xSize / 2 - 25, y + 102 - 2, 50, 20, "Enchant");
        enchantButton.enabled = false;
        buttonList.add(enchantButton);

        if (!checkedForRecipe) {
            container.inventoryChanged();
            checkedForRecipe = true;
        }
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == 0) {
            PacketHandler.sendToServer(new TryEnchantPacket(tile.xCoord, tile.yCoord, tile.zCoord,
                    ExtendedEnchanting.proxy.getPlayer().dimension));
            clearRecipePreview();
            animationTime = animationTicks;
            //Minecraft.getMinecraft().theWorld.playSoundAtEntity(Minecraft.getMinecraft().thePlayer, "", 1, 1);
            /*
            EntityPlayer player = Minecraft.getMinecraft().thePlayer;
            ISound sound = PositionedSoundRecord.func_147675_a(new ResourceLocation("block.fire.extinguish"), (float) player.posX, (float) player.posY, (float) player.posZ);
            Minecraft.getMinecraft().getSoundHandler().playSound(sound);
            */
        }
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int param1, int param2) {
        //fontRendererObj.drawString("gui.enchanting_table", 8, ySize - 96 + 2, 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float timeElapsed, int p_146976_2_, int p_146976_3_) {
        this.mc.getTextureManager().bindTexture(IconInfo.ENCHANTING_TABLE_GUI_RL);
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);

        this.mc.getTextureManager().bindTexture(IconInfo.ENCHANTING_OVERLAY_RL);

        if (animationTime > 0) {
            GL11.glPushMatrix();

            float a = (animationTime - 50F) / 12.5F;
            float alpha = (-1F * a * a + 16F) / 16F;

            alpha *= 0.8F;

            //float alpha = (float) Math.exp(-1d * (Math.pow(((animationTime - 50d) / 25d), 2)));

            GL11.glEnable(GL11.GL_BLEND);
            GL11.glColor4f(1, 1, 1, alpha);
            this.drawTexturedModalRect(x + 47, y + 16, 82, 0, 82, 82);
            GL11.glColor4f(1, 1, 1, 1F);
            this.drawTexturedModalRect(x + 47, y + 16, 0, 0, 82, 82);
            GL11.glDisable(GL11.GL_BLEND);

            animationTime -= timeElapsed;

            GL11.glPopMatrix();
        }




        if (requiredLevels != -1) {
            String s = "" + requiredLevels;
            fontRendererObj.drawStringWithShadow(s, x + xSize / 2 - getStringWidth(s) / 2, y + 7, hasLevel ? 0x00FF00 : 0xFF0000);
        }
    }

    private int getStringWidth(String s) {
        return s != null && !s.equals("") ? fontRendererObj.getStringWidth(EnumChatFormatting.getTextWithoutFormattingCodes(s)) : 0;
    }

    public void previewRecipe(EnchantRecipe recipe) {
        requiredLevels = recipe.getRequiredLevels();
        enchantButton.enabled = hasLevel = recipe.hasExp(Minecraft.getMinecraft().thePlayer);
    }

    public void clearRecipePreview() {
        requiredLevels = -1;
        enchantButton.enabled = false;
    }
}
