package letiu.extended_enchanting.integration.nei;

import codechicken.lib.gui.GuiDraw;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;
import letiu.extended_enchanting.recipes.EnchantRecipe;
import letiu.extended_enchanting.recipes.EnchantRecipeCollector;
import letiu.extended_enchanting.render.IconInfo;
import letiu.extended_enchanting.util.Vector2;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.List;

public class EnchantRecipeHandler extends TemplateRecipeHandler {

    private static final Vector2 resultPos = new Vector2(128, 45);
    private static final Vector2 basePos = new Vector2(50, 45);

    private static Vector2 getPosForIngredient(int index) {
        int spacingA = 28;
        int spacingB = 22;

        switch (index) {
            case 0: return new Vector2(-spacingA, 0).add(basePos);
            case 1: return new Vector2(-spacingB, -spacingB).add(basePos);
            case 2: return new Vector2(0, -spacingA).add(basePos);
            case 3: return new Vector2(spacingB, -spacingB).add(basePos);
            case 4: return new Vector2(spacingA, 0).add(basePos);
            case 5: return new Vector2(spacingB, spacingB).add(basePos);
            case 6: return new Vector2(0, spacingA).add(basePos);
            case 7: return new Vector2(-spacingB, spacingB).add(basePos);
        }

        return null;
    }

    private static PositionedStack createPositionedStack(ItemStack stack, Vector2 pos) {
        return new PositionedStack(stack, pos.x, pos.y);
    }

    public class CachedEnchantRecipe extends CachedRecipe {

        public EnchantRecipe recipe;
        public ItemStack baseOverride;
        public ItemStack resultOverride;

        public CachedEnchantRecipe(EnchantRecipe recipe) {
            this.recipe = recipe;
        }

        public CachedEnchantRecipe(EnchantRecipe recipe, ItemStack baseOverride, ItemStack resultOverride) {
            this(recipe);
            this.baseOverride = baseOverride;
            this.resultOverride = resultOverride;
        }

        @Override
        public List<PositionedStack> getIngredients() {
            ArrayList<PositionedStack> ingredients = new ArrayList<PositionedStack>();

            if (baseOverride != null) ingredients.add(createPositionedStack(baseOverride, basePos));
            else ingredients.add(createPositionedStack(recipe.getBase(), basePos));

            int index = 0;
            for (ItemStack stack : recipe.getCatalysts()) {
                ingredients.add(createPositionedStack(stack, getPosForIngredient(index++)));
            }

            return ingredients;
        }

        @Override
        public PositionedStack getResult() {
            if (resultOverride != null) {
                return createPositionedStack(resultOverride, resultPos);
            }
            else return createPositionedStack(recipe.getResult(), resultPos);
        }
    }

    @Override
    public void loadCraftingRecipes(ItemStack result) {
        int tempStackSize = result.stackSize;
        result.stackSize = 1;
        for (EnchantRecipe recipe : EnchantRecipeCollector.getRecipes()) {
            if (!recipe.isHidden()) {
                if (recipe.getRecipeType() == EnchantRecipe.RecipeType.GENERAL
                        && ItemStack.areItemStacksEqual(result, recipe.getResult())) {

                    arecipes.add(new CachedEnchantRecipe(recipe));
                }
            }
        }
        result.stackSize = tempStackSize;
    }

    @Override
    public void loadUsageRecipes(ItemStack ingredient) {
        int tempStackSize = ingredient.stackSize;
        ingredient.stackSize = 1;
        for (EnchantRecipe recipe : EnchantRecipeCollector.getRecipes()) {
            if (!recipe.isHidden()) {
                if (recipe.getRecipeType() == EnchantRecipe.RecipeType.GENERAL) {
                    if (ItemStack.areItemStacksEqual(recipe.getBase(), ingredient)) {
                        arecipes.add(new CachedEnchantRecipe(recipe));
                    } else {
                        for (ItemStack catalyst : recipe.getCatalysts()) {
                            if (ItemStack.areItemStacksEqual(catalyst, ingredient)) {
                                arecipes.add(new CachedEnchantRecipe(recipe));
                                break;
                            }
                        }
                    }
                } else if (recipe.getRecipeType() == EnchantRecipe.RecipeType.ENCHANT) {
                    Enchantment enchantment = recipe.getEnchantment();
                    if (enchantment.canApply(ingredient) && !EnchantRecipeCollector.hasEnchantment(ingredient, enchantment)) {
                        ItemStack result = ingredient.copy();
                        result.addEnchantment(enchantment, recipe.getEnchantmentLevel());
                        arecipes.add(new CachedEnchantRecipe(recipe, ingredient, result));
                    }
                }
            }
        }
        ingredient.stackSize = tempStackSize;
    }

    @Override
    public int recipiesPerPage() {
        return 1;
    }

    @Override
    public void drawBackground(int recipe) {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GuiDraw.changeTexture(this.getGuiTexture());
        GuiDraw.drawTexturedModalRect(0, 0, 5, 25, 166, 136);
    }

    @Override
    public void drawExtras(int recipeID) {
        EnchantRecipe recipe = ((CachedEnchantRecipe) arecipes.get(recipeID)).recipe;
        GuiDraw.drawStringC("" + recipe.getRequiredLevels(), 136, 30, 0x00FF00);
    }

    @Override
    public String getGuiTexture() {
        return IconInfo.TEXTURE_LOCATION + IconInfo.ENCHANT_RECIPE_GUI;
    }

    @Override
    public String getRecipeName() {
        return "Enchanting Table";
    }
}
