package letiu.extended_enchanting.integration.nei;

import codechicken.nei.api.API;

public class RecipeHandlers {

    public static void registerHandlers() {
        EnchantRecipeHandler enchantRecipeHandler = new EnchantRecipeHandler();
        API.registerRecipeHandler(enchantRecipeHandler);
        API.registerUsageHandler(enchantRecipeHandler);
    }
}
