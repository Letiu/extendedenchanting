package letiu.extended_enchanting.items;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import letiu.extended_enchanting.core.ModInformation;
import net.minecraft.item.Item;

public class ItemData {

    public static Item enchantingCodex;

    public static void init() {

        GameRegistry.registerItem(enchantingCodex = new ItemEnchantingCodex(), ModInformation.ID + "_" + enchantingCodex.getUnlocalizedName());

    }

    public static void addNames() {

        LanguageRegistry.addName(enchantingCodex, enchantingCodex.getUnlocalizedName());

    }

}
