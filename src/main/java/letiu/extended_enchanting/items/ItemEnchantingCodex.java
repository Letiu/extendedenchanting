package letiu.extended_enchanting.items;

import letiu.extended_enchanting.core.ExtendedEnchanting;
import letiu.extended_enchanting.gui.GuiHandler;
import letiu.extended_enchanting.render.IconInfo;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemEnchantingCodex extends Item {

    public ItemEnchantingCodex() {
        this.setCreativeTab(ExtendedEnchanting.modTab);
        this.setMaxStackSize(1);
        this.setUnlocalizedName("Enchanting Codex");
    }

    @Override
    public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
        player.openGui(ExtendedEnchanting.instance, GuiHandler.ENCHANTING_CODEX_ID, world, 0, 0, 0);
        return stack;
    }

    @Override
    public void registerIcons(IIconRegister iconRegister) {
        this.itemIcon = iconRegister.registerIcon(IconInfo.TEXTURE_LOCATION + IconInfo.ENCHANTING_CODEX);
    }
}
