package letiu.extended_enchanting.network;

import cpw.mods.fml.relauncher.Side;
import net.minecraft.entity.player.EntityPlayer;

public class DebugPacket extends LocationPacket<DebugPacket> {

	public DebugPacket() {

	}

	public DebugPacket(int x, int y, int z, int dimID) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.dimID = dimID;
	}
	
	@Override
	public void execute(DebugPacket packet, EntityPlayer player, Side side) {
		if (side == Side.SERVER) {
			PacketHandler.sendToAllInDimension(packet, packet.dimID);
			System.out.println("Server: " + player.openContainer);
		}
		if (side == Side.CLIENT) {
			if (player.dimension == packet.dimID) {
				player.worldObj.markBlockForUpdate(x, y, z);
			}
		}
	}
}
