package letiu.extended_enchanting.network;

import cpw.mods.fml.relauncher.Side;
import letiu.extended_enchanting.config.RecipeFileHandler;
import letiu.extended_enchanting.recipes.EnchantRecipe;
import letiu.extended_enchanting.recipes.EnchantRecipeCollector;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ERecipeListPacket extends CustomPacket<ERecipeListPacket> {

	private List<EnchantRecipe> recipes;

	public ERecipeListPacket() {

	}

	public ERecipeListPacket(List<EnchantRecipe> recipes) {
		this.recipes = recipes;
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		super.write(out);
		NBTTagCompound nbt = new NBTTagCompound();

		NBTTagList nbt_list = new NBTTagList();
		for (EnchantRecipe recipe : recipes) {
		    nbt_list.appendTag(recipe.writeToNBT());
		}

		nbt.setTag("recipe_list", nbt_list);

		CompressedStreamTools.writeCompressed(nbt, out);
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		super.read(in);

		this.recipes = new ArrayList<EnchantRecipe>();

		NBTTagCompound nbt = CompressedStreamTools.readCompressed(in);
		NBTTagList nbt_list = nbt.getTagList("recipe_list", 10);

		for (int i = 0; i < nbt_list.tagCount(); i++) {
			EnchantRecipe recipe = EnchantRecipe.getEmptyRecipe();
			recipe.readFromNBT(nbt_list.getCompoundTagAt(i));
			this.recipes.add(recipe);
		}
	}

	@Override
	public void execute(ERecipeListPacket packet, EntityPlayer player, Side side) {
		if (side == Side.CLIENT) {
			EnchantRecipeCollector.clear();

			for (EnchantRecipe recipe : packet.recipes) {
			    EnchantRecipeCollector.addRecipe(recipe);
			}

			RecipeFileHandler.save();
		}
	}
}
