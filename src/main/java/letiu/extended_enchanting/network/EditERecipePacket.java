package letiu.extended_enchanting.network;

import cpw.mods.fml.relauncher.Side;
import letiu.extended_enchanting.config.RecipeFileHandler;
import letiu.extended_enchanting.recipes.EnchantRecipe;
import letiu.extended_enchanting.recipes.EnchantRecipeCollector;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.CompressedStreamTools;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

public class EditERecipePacket extends CustomPacket<EditERecipePacket> {

	private EnchantRecipe recipe;
	private int recipeID;


	public EditERecipePacket() {

	}

	public EditERecipePacket(EnchantRecipe recipe, int recipeID) {
		this.recipe = recipe;
		this.recipeID = recipeID;
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		super.write(out);
		out.writeInt(recipeID);
		CompressedStreamTools.writeCompressed(recipe.writeToNBT(), out);
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		super.read(in);
		this.recipeID = in.readInt();
		this.recipe = EnchantRecipe.getEmptyRecipe();
		this.recipe.readFromNBT(CompressedStreamTools.readCompressed(in));
	}

	@Override
	public void execute(EditERecipePacket packet, EntityPlayer player, Side side) {
		if (side == Side.SERVER) {
			List<EnchantRecipe> recipes = EnchantRecipeCollector.getRecipes();

			if (packet.recipeID >= recipes.size() || packet.recipeID < 0) {
			    recipes.add(packet.recipe);
			}
			else {
			    recipes.set(packet.recipeID, packet.recipe);
			}

			RecipeFileHandler.save();

			PacketHandler.sendToAllPlayers(new ERecipeListPacket(EnchantRecipeCollector.getRecipes()));
		}
	}
}
