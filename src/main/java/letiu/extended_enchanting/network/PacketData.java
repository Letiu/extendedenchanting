package letiu.extended_enchanting.network;

public class PacketData {

	public static void init() {

		PacketHandler.registerPacket(new TryEnchantPacket());
		PacketHandler.registerPacket(new DebugPacket());
		PacketHandler.registerPacket(new EditERecipePacket());
		PacketHandler.registerPacket(new RemoveERecipePacket());
		PacketHandler.registerPacket(new ERecipeListPacket());
		PacketHandler.registerPacket(new RequestERecipeListPacket());

	}
}
