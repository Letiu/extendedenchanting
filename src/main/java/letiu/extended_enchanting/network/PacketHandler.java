package letiu.extended_enchanting.network;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;
import letiu.extended_enchanting.core.ExtendedEnchanting;
import letiu.extended_enchanting.core.ModInformation;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;

public class PacketHandler {

	private static SimpleNetworkWrapper wrapper = new SimpleNetworkWrapper(ModInformation.CHANNEL);
	
	private static int counter = 0;
	
	
	public static void registerPacket(CustomPacket packet) {
		wrapper.registerMessage((Class<? extends IMessageHandler<CustomPacket, IMessage>>) packet.getClass(), (Class<CustomPacket>) packet.getClass(), counter++, Side.CLIENT);
		wrapper.registerMessage((Class<? extends IMessageHandler<CustomPacket, IMessage>>) packet.getClass(), (Class<CustomPacket>) packet.getClass(), counter++, Side.SERVER);
	}
	
	
	public static void sendToAllInDimension(CustomPacket packet, int dimID) {
		wrapper.sendToDimension(packet, dimID);
	}

	public static void sendToAllPlayers(CustomPacket packet) {
	    wrapper.sendToAll(packet);
	}

	public static void sendToServer(CustomPacket packet) {
		wrapper.sendToServer(packet);
	}

	public static void sendToPlayer(CustomPacket packet, EntityPlayerMP player) {
	    wrapper.sendTo(packet, player);
	}

}
