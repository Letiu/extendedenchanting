package letiu.extended_enchanting.network;

import cpw.mods.fml.relauncher.Side;
import letiu.extended_enchanting.config.RecipeFileHandler;
import letiu.extended_enchanting.recipes.EnchantRecipe;
import letiu.extended_enchanting.recipes.EnchantRecipeCollector;
import net.minecraft.entity.player.EntityPlayer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

public class RemoveERecipePacket extends CustomPacket<RemoveERecipePacket> {

	private int recipeID;

	public RemoveERecipePacket() {

	}

	public RemoveERecipePacket(int recipeID) {
		this.recipeID = recipeID;
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		super.write(out);
		out.writeInt(recipeID);
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		super.read(in);
		this.recipeID = in.readInt();
	}

	@Override
	public void execute(RemoveERecipePacket packet, EntityPlayer player, Side side) {
		if (side == Side.SERVER) {
			List<EnchantRecipe> recipes = EnchantRecipeCollector.getRecipes();

			if (packet.recipeID < recipes.size() && packet.recipeID >= 0) {
				recipes.remove(packet.recipeID);
			}

			RecipeFileHandler.save();
		}
	}
}
