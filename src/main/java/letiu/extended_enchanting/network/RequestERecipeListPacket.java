package letiu.extended_enchanting.network;

import cpw.mods.fml.relauncher.Side;
import letiu.extended_enchanting.recipes.EnchantRecipeCollector;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;

public class RequestERecipeListPacket extends CustomPacket<RequestERecipeListPacket> {

	public RequestERecipeListPacket() {

	}

	@Override
	public void execute(RequestERecipeListPacket packet, EntityPlayer player, Side side) {
		if (side == Side.SERVER) {
			PacketHandler.sendToPlayer(new ERecipeListPacket(EnchantRecipeCollector.getRecipes()), (EntityPlayerMP) player);
		}
	}
}
