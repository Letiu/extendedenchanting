package letiu.extended_enchanting.network;

import cpw.mods.fml.relauncher.Side;
import letiu.extended_enchanting.tiles.TileEnchantingTable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;

public class TryEnchantPacket extends LocationPacket<TryEnchantPacket> {

	public TryEnchantPacket() {

	}

	public TryEnchantPacket(int x, int y, int z, int dimID) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.dimID = dimID;
	}
	
	@Override
	public void execute(TryEnchantPacket packet, EntityPlayer player, Side side) {
		if (side == Side.SERVER) {
			// TODO: check if other dimensions work
			TileEntity tile = player.worldObj.getTileEntity(packet.x, packet.y, packet.z);

			if (tile != null && tile instanceof TileEnchantingTable) {
				((TileEnchantingTable) tile).tryEnchant(player);
			}
		}
	}
}
