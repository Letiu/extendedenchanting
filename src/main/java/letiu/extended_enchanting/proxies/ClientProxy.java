package letiu.extended_enchanting.proxies;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;

public class ClientProxy extends CommonProxy {

	@Override
	public void initSounds() {
		
	}

	@Override
	public void initRenderes() {

	}

	public void registerEvents() {
		super.registerEvents();
	}
	
	@Override
	public EntityPlayer getPlayer() {
		return Minecraft.getMinecraft().thePlayer;
	}
}
