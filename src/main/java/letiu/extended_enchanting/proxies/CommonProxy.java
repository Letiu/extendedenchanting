package letiu.extended_enchanting.proxies;

import com.mojang.authlib.GameProfile;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.registry.GameRegistry;
import letiu.extended_enchanting.core.ModInformation;
import letiu.extended_enchanting.core.Recipes;
import letiu.extended_enchanting.enchantments.PotionEffectTickHandler;
import letiu.extended_enchanting.tiles.TileEnchantingTable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.util.FakePlayerFactory;

import java.util.UUID;


public class CommonProxy {
	
	public void initSounds() {
		
	}

	public void initRenderes() {
		
	}
	
	public void registerEvents() {
		FMLCommonHandler.instance().bus().register(new PotionEffectTickHandler());
	}

	public void registerTileEntities() {
		GameRegistry.registerTileEntity(TileEnchantingTable.class, ModInformation.ID + "_enchanting_table");
	}

	public void addRecipes() {
		Recipes.registerNormalRecipes();
	}

	public EntityPlayer getPlayer() { return null; }

}
