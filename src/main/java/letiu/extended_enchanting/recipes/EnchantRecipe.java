package letiu.extended_enchanting.recipes;

import letiu.extended_enchanting.core.ExtendedEnchanting;
import letiu.extended_enchanting.util.CompareUtil;
import letiu.extended_enchanting.util.NBTUtil;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;

public class EnchantRecipe {

    private ItemStack result, base;
    private ItemStack[] catalysts;
    private int requiredLevels;
    private String name;
    private RecipeType recipeType;
    private int enchStrength;
    private boolean isHidden;
    private ItemStack[] blackWhiteList = new ItemStack[18];
    private boolean isWhitelist;

    private boolean[] armorLocks = new boolean[4];

    public enum RecipeType {
        GENERAL, ENCHANT, POTION;
    }

    public EnchantRecipe(String name, ItemStack result, ItemStack base, int requiredLevels, RecipeType recipeType, boolean isHidden, ItemStack... catalysts) {
        this.result = result;
        this.base = base;
        this.requiredLevels = requiredLevels;
        this.catalysts = catalysts;
        this.name = name;
        this.recipeType = recipeType;
        this.enchStrength = 1;
        this.isHidden = isHidden;
    }

    public EnchantRecipe(String name, ItemStack result, ItemStack base, int requiredLevels, RecipeType recipeType, boolean isHidden, int enchStrength, ItemStack... catalysts) {
        this(name, result, base, requiredLevels, recipeType, isHidden, catalysts);
        this.enchStrength = enchStrength;
    }

    public EnchantRecipe(String name, ItemStack result, ItemStack base, int requiredLevels, RecipeType recipeType, boolean isHidden,
                         int enchStrength, boolean isWhitelist, ItemStack[] blackWhiteList, boolean[] armorLocks, ItemStack... catalysts) {
        this(name, result, base, requiredLevels, recipeType, isHidden, enchStrength, catalysts);

        this.isWhitelist = isWhitelist;
        this.blackWhiteList = blackWhiteList;
        this.armorLocks = armorLocks;
    }

    public ItemStack getBase() {
        return base;
    }

    public ItemStack getResult() {
        return result;
    }

    public ItemStack[] getCatalysts() {
        return catalysts;
    }

    public int getRequiredLevels() {
        return requiredLevels;
    }

    public String getName() {
        return name;
    }

    public RecipeType getRecipeType() {
        return recipeType;
    }

    public boolean isHidden() {
        return isHidden;
    }

    public int getEnchStrength() {
        return enchStrength;
    }

    public boolean isWhitelist() {
        return isWhitelist;
    }

    public ItemStack[] getBlackWhiteList() {
        return blackWhiteList;
    }

    public boolean[] getArmorLocks() {
        return armorLocks;
    }

    public Enchantment getEnchantment() {
        if (recipeType == RecipeType.POTION) {
            return ExtendedEnchanting.POTION_ENCHANTMENT;
        }
        else if (recipeType == RecipeType.ENCHANT) {
            NBTTagList nbt_list = Items.enchanted_book.func_92110_g(result);
            return Enchantment.enchantmentsList[nbt_list.getCompoundTagAt(0).getShort("id")];
        }
        else {
            return null;
        }
    }

    public int getEnchantmentLevel() {

        if (recipeType == RecipeType.POTION) {
            //NBTTagList nbt_list = Items.enchanted_book.func_92110_g(result);
            //return nbt_list.getCompoundTagAt(0).getShort("lvl");
            return enchStrength;
        }

        if (recipeType == RecipeType.ENCHANT) return enchStrength;

        return 0;
    }

    public boolean hasExp(EntityPlayer player) {
        return player.experienceLevel >= requiredLevels || player.capabilities.isCreativeMode;
    }

    public boolean checkBlackWhitelist(ItemStack stack) {

        if (stack != null && stack.getItem() != null && stack.getItem() instanceof ItemArmor) {
            int armorType = ((ItemArmor) stack.getItem()).armorType;
            if (armorLocks[armorType]) return false;
        }

        for (ItemStack entry : blackWhiteList) {
            if (CompareUtil.compareIDs(entry, stack)) return isWhitelist;
        }
        return !isWhitelist;
    }

    public NBTTagCompound writeToNBT() {
        NBTTagCompound nbt = new NBTTagCompound();

        nbt.setString("name", name);
        nbt.setInteger("requiredLevels", requiredLevels);

        nbt.setTag("result", NBTUtil.writeStackToNBTWithName(result));
        nbt.setTag("base", NBTUtil.writeStackToNBTWithName(base));

        NBTTagList nbt_catalysts = new NBTTagList();

        for (ItemStack catalyst : catalysts) {
            nbt_catalysts.appendTag(NBTUtil.writeStackToNBTWithName(catalyst));
        }

        nbt.setTag("catalysts", nbt_catalysts);

        nbt.setByte("recipeType", (byte) recipeType.ordinal());
        nbt.setShort("enchStrength", (short) enchStrength);
        nbt.setBoolean("isHidden", isHidden);

        nbt.setBoolean("isWhitelist", isWhitelist);

        if (blackWhiteList != null && blackWhiteList.length > 0) {
            NBTTagList nbt_blackWhitelist = new NBTTagList();
            for (ItemStack listentry : blackWhiteList) {
                if (listentry != null) {
                    nbt_blackWhitelist.appendTag(NBTUtil.writeStackToNBTWithName(listentry));
                }
            }
            nbt.setTag("blackWhitelist", nbt_blackWhitelist);
        }

        nbt.setBoolean("armorLock0", armorLocks[0]);
        nbt.setBoolean("armorLock1", armorLocks[1]);
        nbt.setBoolean("armorLock2", armorLocks[2]);
        nbt.setBoolean("armorLock3", armorLocks[3]);

        return nbt;
    }

    public void readFromNBT(NBTTagCompound nbt) {
        this.name = nbt.getString("name");
        this.requiredLevels = nbt.getInteger("requiredLevels");

        this.result = NBTUtil.readStackFromNBTWithName(nbt.getCompoundTag("result"));
        this.base = NBTUtil.readStackFromNBTWithName(nbt.getCompoundTag("base"));

        NBTTagList nbt_catalysts = nbt.getTagList("catalysts", 10);
        catalysts = new ItemStack[nbt_catalysts.tagCount()];

        for (int i = 0; i < nbt_catalysts.tagCount(); i++) {
            catalysts[i] = NBTUtil.readStackFromNBTWithName(nbt_catalysts.getCompoundTagAt(i));
        }

        if (nbt.hasKey("recipeType")) {
            recipeType = RecipeType.values()[nbt.getByte("recipeType")];
        }
        else {
            recipeType = RecipeType.GENERAL;
        }

        if (nbt.hasKey("enchStrength")) {
            int id = nbt.getTag("enchStrength").getId();
            if (Constants.NBT.TAG_BYTE == id) {
                enchStrength = nbt.getByte("enchStrength");
            }
            else if (Constants.NBT.TAG_SHORT == id) {
                enchStrength = nbt.getShort("enchStrength");
            }

        } else {
            enchStrength = 1;
        }

        if (nbt.hasKey("isHidden")) {
            isHidden = nbt.getBoolean("isHidden");
        } else {
            isHidden = false;
        }

        if (nbt.hasKey("isWhitelist")) {
            isWhitelist = nbt.getBoolean("isWhitelist");
        }
        else {
            isWhitelist = false;
        }

        if (nbt.hasKey("blackWhitelist")) {
            NBTTagList nbt_blackWhitelist = nbt.getTagList("blackWhitelist", 10);
            blackWhiteList = new ItemStack[nbt_blackWhitelist.tagCount()];

            for (int i = 0; i < nbt_blackWhitelist.tagCount(); i++) {
                blackWhiteList[i] = NBTUtil.readStackFromNBTWithName(nbt_blackWhitelist.getCompoundTagAt(i));
            }
        }
        else {
            blackWhiteList = new ItemStack[18];
        }

        if (nbt.hasKey("armorLock0")) armorLocks[0] = nbt.getBoolean("armorLock0");
        if (nbt.hasKey("armorLock1")) armorLocks[1] = nbt.getBoolean("armorLock1");
        if (nbt.hasKey("armorLock2")) armorLocks[2] = nbt.getBoolean("armorLock2");
        if (nbt.hasKey("armorLock3")) armorLocks[3] = nbt.getBoolean("armorLock3");

    }

    private EnchantRecipe() {}

    public static EnchantRecipe getEmptyRecipe() {
        return new EnchantRecipe();
    }

}
