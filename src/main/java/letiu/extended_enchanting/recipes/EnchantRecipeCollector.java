package letiu.extended_enchanting.recipes;

import letiu.extended_enchanting.core.ExtendedEnchanting;
import letiu.extended_enchanting.util.CompareUtil;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemEnchantedBook;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

import java.util.ArrayList;
import java.util.List;

public class EnchantRecipeCollector {

    private static ArrayList<EnchantRecipe> recipes = new ArrayList<EnchantRecipe>();

    private static void loadDefaultRecipes() {
        recipes.add(new EnchantRecipe("Coal->Diamond",
                                      new ItemStack(Items.diamond),
                                      new ItemStack(Items.coal),
                                      1,
                                      EnchantRecipe.RecipeType.GENERAL,
                                      false,
                                      new ItemStack(Items.gold_nugget),
                                      new ItemStack(Items.gold_nugget)));
        ItemStack stack = new ItemStack(Items.diamond_sword);
        stack.addEnchantment(Enchantment.sharpness, 4);
        recipes.add(new EnchantRecipe("DiaSword+=Sharp4",
                stack,
                new ItemStack(Items.diamond_sword),
                20,
                EnchantRecipe.RecipeType.GENERAL,
                false,
                new ItemStack(Items.quartz),
                new ItemStack(Items.quartz),
                new ItemStack(Items.quartz),
                new ItemStack(Items.quartz)));
    }

    public static void addRecipe(EnchantRecipe recipe) {
        recipes.add(recipe);
    }

    public static EnchantRecipe findMatch(ItemStack[] stacks, EntityPlayer player, boolean mindLevel) {

        for (EnchantRecipe recipe : recipes) {
            if ((!mindLevel || recipe.hasExp(player))) {
                if ((recipe.getRecipeType() == EnchantRecipe.RecipeType.GENERAL && checkRecipe(recipe, stacks))
                        || (recipe.getRecipeType() == EnchantRecipe.RecipeType.ENCHANT
                        || recipe.getRecipeType() == EnchantRecipe.RecipeType.POTION) && canEnchant(recipe, stacks)) {
                    return recipe;
                }
            }
        }

        return null;
    }

    private static boolean canEnchant(EnchantRecipe recipe, ItemStack[] stacks) {

        if (stacks[0] == null || !compareCatalysts(recipe, stacks)) return false;

        Enchantment enchantment = recipe.getEnchantment();
        return (!hasEnchantment(stacks[0], enchantment) || enchantment == ExtendedEnchanting.POTION_ENCHANTMENT)
                && enchantment.canApply(stacks[0]) && recipe.checkBlackWhitelist(stacks[0]);
    }

    public static boolean hasEnchantment(ItemStack stack, Enchantment enchantment) {

        if (stack.stackTagCompound == null) return false;

        NBTTagList nbt_list = stack.stackTagCompound.getTagList("ench", 10);

        for (int i = 0; i < nbt_list.tagCount(); i++) {
            int id = nbt_list.getCompoundTagAt(i).getShort("id");
            if (id == enchantment.effectId) return true;
        }

        return false;
    }

    private static boolean checkRecipe(EnchantRecipe recipe, ItemStack[] stacks) {

        if (!CompareUtil.compareIDs(stacks[0], recipe.getBase())) {
            return false;
        }

        return compareCatalysts(recipe, stacks);
    }

    private static boolean compareCatalystsShaped(EnchantRecipe recipe, ItemStack[] stacks) {
        ItemStack[] catalysts = recipe.getCatalysts();

        for (int i = 0; i < catalysts.length; i++) {

        }

        // dummy
        return false;
    }

    private static boolean compareCatalysts(EnchantRecipe recipe, ItemStack[] stacks) {
        ItemStack[] catalysts = recipe.getCatalysts();
        boolean[] catalystsChecked = new boolean[catalysts.length];

        for (int i = 0; i < catalysts.length; i++) {
            if (catalysts[i] == null) {
                catalystsChecked[i] = true;
            }
        }

        for (int i = 1; i < stacks.length; i++) {
            if (stacks[i] != null) {
                boolean matchFound = false;
                for (int k = 0; k < catalysts.length; k++) {
                    if (!catalystsChecked[k] && CompareUtil.compareIDs(stacks[i], catalysts[k])) {
                        catalystsChecked[k] = matchFound = true;
                        break;
                    }
                }
                if (!matchFound) {
                    return false;
                }
            }
        }

        for (int i = 0; i < catalystsChecked.length; i++) {
            if (!catalystsChecked[i]) {
                return false;
            }
        }

        return true;
    }

    public static List<EnchantRecipe> getRecipes() {
        return recipes;
    }

    public static void clear() {
        recipes.clear();
    }
}
