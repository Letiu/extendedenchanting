package letiu.extended_enchanting.render;

import letiu.extended_enchanting.core.ModInformation;
import net.minecraft.util.ResourceLocation;

public class IconInfo {

    public static final String TEXTURE_LOCATION = "extended_enchanting:";

    // BLOCKS //
    public static final String ENCHANTING_TABLE_SIDE = "enchanting_table_side";
    public static final String ENCHANTING_TABLE_TOP = "enchanting_table_top";
    public static final String ENCHANTING_TABLE_BOTTOM = "enchanting_table_bottom";

    // ITEMS //
    public static final String ENCHANTING_CODEX = "enchanting_codex";

    // OTHER TEXTURES //
    public static final String ENCHANTING_TABLE_GUI = "textures/gui/enchanting_table_gui.png";
    public static final ResourceLocation ENCHANTING_TABLE_GUI_RL = new ResourceLocation(ModInformation.ID, ENCHANTING_TABLE_GUI);

    public static final String ENCHANTING_CODEX_GUI = "textures/gui/empty_gui.png";
    public static final ResourceLocation ENCHANTING_CODEX_GUI_RL = new ResourceLocation(ModInformation.ID, ENCHANTING_CODEX_GUI);

    public static final String CODEX_CREATE_GUI = "textures/gui/codex_create_gui.png";
    public static final ResourceLocation CODEX_CREATE_GUI_RL = new ResourceLocation(ModInformation.ID, CODEX_CREATE_GUI);

    public static final String ENCHANT_RECIPE_GUI = "textures/gui/enchant_recipe_gui.png";
    public static final ResourceLocation ENCHANT_RECIPE_GUI_RL = new ResourceLocation(ModInformation.ID, ENCHANT_RECIPE_GUI);

    public static final String ENCHANTING_OVERLAY = "textures/gui/enchanting_overlay.png";
    public static final ResourceLocation ENCHANTING_OVERLAY_RL = new ResourceLocation(ModInformation.ID, ENCHANTING_OVERLAY);

    public static final String CODEX_CREATE_ENCHANT_GUI = "textures/gui/codex_create_enchant_gui.png";
    public static final ResourceLocation CODEX_CREATE_ENCHANT_GUI_RL = new ResourceLocation(ModInformation.ID, CODEX_CREATE_ENCHANT_GUI);

    public static final String CODEX_CREATE_POTION_GUI = "textures/gui/codex_create_potion_gui.png";
    public static final ResourceLocation CODEX_CREATE_POTION_GUI_RL = new ResourceLocation(ModInformation.ID, CODEX_CREATE_POTION_GUI);

    public static final String BUTTON_ICONS = "textures/gui/buttons.png";
    public static final ResourceLocation BUTTON_ICONS_RL = new ResourceLocation(ModInformation.ID, BUTTON_ICONS);

    public static final String BLACK_WHITE_LIST_GUI = "textures/gui/black_white_list_gui.png";
    public static final ResourceLocation BLACK_WHITE_LIST_GUI_RL = new ResourceLocation(ModInformation.ID, BLACK_WHITE_LIST_GUI);

}
