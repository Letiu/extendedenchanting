package letiu.extended_enchanting.tiles;

import letiu.extended_enchanting.blocks.BlockData;
import letiu.extended_enchanting.enchantments.PotionEnchantment;
import letiu.extended_enchanting.recipes.EnchantRecipe;
import letiu.extended_enchanting.recipes.EnchantRecipeCollector;
import letiu.extended_enchanting.util.CompareUtil;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityEnchantmentTable;

public class TileEnchantingTable extends TileEntity implements IInventory {

    private ItemStack[] inventory = new ItemStack[9];

    public void tryEnchant(EntityPlayer player) {
        EnchantRecipe recipe = EnchantRecipeCollector.findMatch(inventory, player, true);

        if (recipe != null) {
            player.addExperienceLevel(-recipe.getRequiredLevels());

            if (recipe.getRecipeType() == EnchantRecipe.RecipeType.GENERAL) {
                clearInventory();
                inventory[0] = recipe.getResult().copy();
            }
            else if (recipe.getRecipeType() == EnchantRecipe.RecipeType.ENCHANT) {
                ItemStack stack = inventory[0];
                clearInventory();
                stack.addEnchantment(recipe.getEnchantment(), recipe.getEnchantmentLevel());
                inventory[0] = stack;
            }
            else if (recipe.getRecipeType() == EnchantRecipe.RecipeType.POTION) {
                ItemStack stack = inventory[0];
                clearInventory();
                PotionEnchantment.addEnchantment(stack, recipe.getEnchStrength());
                inventory[0] = stack;
            }
        }
    }

    private void clearInventory() {
        for (int i = 0; i < inventory.length; i++) {
            inventory[i] = null;
        }
    }

    public ItemStack[] getInventory() {
        return inventory;
    }

    @Override
    public void writeToNBT(NBTTagCompound tagCompound) {
        super.writeToNBT(tagCompound);

        NBTTagList nbttaglist = new NBTTagList();

            for (int i = 0; i < this.inventory.length; ++i)
            {
                if (this.inventory[i] != null)
                {
                    NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                    nbttagcompound1.setByte("Slot", (byte)i);
                    this.inventory[i].writeToNBT(nbttagcompound1);
                    nbttaglist.appendTag(nbttagcompound1);
                }
            }

            tagCompound.setTag("Items", nbttaglist);
    }

    @Override
    public void readFromNBT(NBTTagCompound tagCompound) {
        super.readFromNBT(tagCompound);

        NBTTagList nbttaglist = tagCompound.getTagList("Items", 10);
        this.inventory = new ItemStack[this.getSizeInventory()];

        for (int i = 0; i < nbttaglist.tagCount(); ++i) {
            NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
            int j = nbttagcompound1.getByte("Slot") & 255;

            if (j >= 0 && j < this.inventory.length) {
                this.inventory[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
            }
        }
    }

    @Override
    public Packet getDescriptionPacket() {
        super.getDescriptionPacket();
        NBTTagCompound tag = new NBTTagCompound();
        this.writeToNBT(tag);
        return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, tag);
    }

    @Override
    public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity packet) {
        readFromNBT(packet.func_148857_g());
    }

    @Override
    public int getSizeInventory() {
        return inventory.length;
    }

    @Override
    public ItemStack getStackInSlot(int i) {
        if (i < 0 || i >= inventory.length) return null;
        return inventory[i];
    }

    @Override
    public ItemStack decrStackSize(int i, int amt) {
        if (i < 0 || i >= inventory.length) return null;

        ItemStack stack = getStackInSlot(i);

        if (stack != null) {
            if (stack.stackSize <= amt) {
                setInventorySlotContents(i, null);
            }
            else {
                stack = stack.splitStack(amt);
                if (stack.stackSize == 0) {
                    setInventorySlotContents(i, null);
                }
            }
        }

        return stack;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int i) {
        return getStackInSlot(i);
    }

    @Override
    public void setInventorySlotContents(int i, ItemStack stack) {
        if (i < 0 || i >= inventory.length) return;
        inventory[i] = stack;
        if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
            stack.stackSize = this.getInventoryStackLimit();
        }
    }

    @Override
    public String getInventoryName() {
        return BlockData.enchantingTable.getUnlocalizedName();
    }

    @Override
    public boolean hasCustomInventoryName() {
        return false;
    }

    @Override
    public int getInventoryStackLimit() {
        return 1;
    }

    public boolean isUseableByPlayer(EntityPlayer player) {
        return true;
    }

    @Override
    public void openInventory() {

    }

    @Override
    public void closeInventory() {

    }

    @Override
    public boolean isItemValidForSlot(int i, ItemStack stack) {
        return i >= 0 && i < inventory.length;
    }


}
