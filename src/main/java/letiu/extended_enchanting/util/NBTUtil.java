package letiu.extended_enchanting.util;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class NBTUtil {

    public static NBTTagCompound writeStackToNBTWithName(ItemStack stack) {
        NBTTagCompound nbt = new NBTTagCompound();

        stack.writeToNBT(nbt);
        nbt.setString("itemName", Item.itemRegistry.getNameForObject(stack.getItem()));

        return nbt;
    }

    public static ItemStack readStackFromNBTWithName(NBTTagCompound nbt) {

        ItemStack stack = ItemStack.loadItemStackFromNBT(nbt);
        Item item = (Item) Item.itemRegistry.getObject(nbt.getString("itemName"));

        if (stack != null) {
            if (item != null && stack.getItem() != item) {
                ItemStack fixedStack = new ItemStack(item);
                fixedStack.setItemDamage(stack.getItemDamage());
                fixedStack.stackSize = stack.stackSize;
                fixedStack.stackTagCompound = stack.stackTagCompound;
                stack = fixedStack;
            }
        }
        else if (item != null) {
            System.out.println("EXTENDED_ENCHANTING: loading ItemStack by name");
            System.out.println(nbt);
            stack = new ItemStack(item);
        }
        else {
            System.out.println("EXTENDED_ENCHANTING_ERROR: couldn't read ItemStack from nbt");
            System.out.println(nbt);
        }

        return stack;
    }


}
