package letiu.extended_enchanting.util;

public class NumberMagic {

    public static int getPotionID(int num) {
        return num & 2047; // 2 ^ 11
    }

    public static int getPotionStrength(int num) {
        int result = num >> 11;
        //// There are 4 bit remaining (coming from a short) everything above 8 is converted to negative
        //if (result < 0) result = 8 + result * -1;
        return result;
    }

    public static int setPotionStrength(int enchantStrength, int newPotStrength) {
        return getPotionID(enchantStrength) | (newPotStrength << 11);
    }

    public static int getEnchantStrength(int potionID, int potionStrength) {
        return (potionID & 2047) | (potionStrength << 11);
    }
}
